STEP PROJECT 3

- Moskalenko Anton
- Igor Petrov
- Illa Makarov

Tasks of each member of the project

# Moskalenko Anton
Header (шапка) сторінки(верстка). Розробка класу Modal та інтерфейсу модального вікна.  
   
   
# Igor Petrov
Форма для фільтрації візитів(верстка). Розробка класу Visit та його дочірніх класів.  


# Illa Makarov
Список створених візитів(верстка). Фільтри візитів та взаємодія з сервером.
 